#!/usr/bin/env python
# Copyright European Organization for Nuclear Research (CERN)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# You may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# http://www.apache.org/licenses/LICENSE-2.0
#
# Authors:
# - Vincent Garonne, <vincent.garonne@cern.ch>, 2013-2014
# - Mario Lassnig, <mario.lassnig@cern.ch>, 2014
# - Cedric Serfon, <cedric.serfon@cern.ch>, 2014
# - David Cameron, <david.cameron@cern.ch>, 2015

import json
import os.path
import requests
import sys
import traceback
import urlparse


from rucio.api.account import list_accounts, list_account_attributes
from rucio.api.account_limit import set_account_limit
from rucio.client import Client
from rucio.common.exception import Duplicate, RSEProtocolPriorityError


UNKNOWN = 3
CRITICAL = 2
WARNING = 1
OK = 0

if __name__ == '__main__':

    url = 'http://atlas-agis-api.cern.ch/request/ddmendpoint/query/list/?json&state=ACTIVE&site_state=ACTIVE'
    resp = requests.get(url=url)
    data = json.loads(resp.content)
    retvalue = OK

    c = Client()
    for rse in data:
        try:
            deterministic = rse['is_deterministic']
            volatile = False
            c.add_rse(rse=rse['name'], deterministic=deterministic, volatile=volatile)
            # Set infinite quotas for admin accounts
            acc_attrs = [(a['account'], list_account_attributes(a['account'])) for a in list_accounts()]
            admin_accounts = [x[0] for x in acc_attrs if {'key': 'admin', 'value': True} in x[1]]
            for account in admin_accounts:
                set_account_limit(account, rse['name'], -1, 'root')
            if not rse['is_tape']:
                set_account_limit('sonar', rse['name'], 10000000000000, 'root')
        except Duplicate, e:
            pass
        except:
            retvalue = CRITICAL
            errno, errstr = sys.exc_info()[:2]
            trcbck = traceback.format_exc()
            print 'Interrupted processing with %s %s %s.' % (errno, errstr, trcbck)

        prefix = rse['endpoint']
        space_token = rse['token']

        for protocol in rse['protocols']:
            try:
                o = urlparse.urlparse(protocol)
                if o.scheme not in ('https', 'http', 'srm', 'gsiftp', 'root'):
                    continue

                priority = rse['protocols'][protocol][0][1]
                extended_attributes = None
                if o.scheme == 'srm':
                    extended_attributes = {"web_service_path": o.path + '?SFN=', "space_token": space_token}
                    impl = 'rucio.rse.protocols.gfal.Default'
                elif o.scheme == 'https' or o.scheme == 'http':
                    extended_attributes = None
                    impl = 'rucio.rse.protocols.webdav.Default'
                elif o.scheme == 'gsiftp':
                    extended_attributes = None
                    impl = 'rucio.rse.protocols.gsiftp.Default'
                elif o.scheme == 'root':
                    extended_attributes = None
                    impl = 'rucio.rse.protocols.xrootd.Default'
                else:
                    continue

                netloc = o.netloc
                if o.port and str(o.port) in o.netloc:
                    netloc = o.netloc[:-len(':' + str(o.port))]

                # For disk end-points not for tape
                prefix = rse['protocols'][protocol][0][2]
                if not rse['is_tape'] and not prefix.endswith('/rucio') and not prefix.endswith('/rucio/'):
                    prefix = os.path.join(prefix, 'rucio/')

                params = {'hostname': netloc,
                          'scheme': o.scheme,
                          'port': o.port or 443,
                          'prefix': prefix,
                          'impl': impl,
                          'extended_attributes': extended_attributes,
                          'domains': {"lan": {"read": priority,
                                              "write": priority,
                                              "delete": priority},
                                      "wan": {"read": priority,
                                              "write": priority,
                                              "delete": priority}}}

                c.add_protocol(rse=rse['name'], params=params)
            except Duplicate, e:
                pass
            except RSEProtocolPriorityError, e:
                print 'RSE %s protocol %s: %s' % (rse['name'], o.scheme, e)
                if retvalue != CRITICAL:
                    retvalue = WARNING
            except:
                retvalue = CRITICAL
                errno, errstr = sys.exc_info()[:2]
                trcbck = traceback.format_exc()
                print 'RSE %s protocol %s : Interrupted processing with %s %s %s.' % (rse['name'], o.scheme, errno, errstr, trcbck)

    sys.exit(retvalue)
